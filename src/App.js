import Compontent from "./components/component";
import {SelectableGroup} from 'react-selectable-fast'

function App() {
  const handleSelecting = (item) => {
    console.log('selecting'+item);
  }
  const handleSelectionClear = (item) => {
    console.log('clear'+item);
  }
  const handleSelectionFinish = (item) =>{
    console.log('selected'+item);
  }
  return (
    <div className="App">
      <SelectableGroup className="main"
        clickClassName="tick"
        enableDeselect
        allowClickWithoutSelected={false} 
        duringSelection={handleSelecting}
        onSelectionClear={handleSelectionClear}
        onSelectionFinish={handleSelectionFinish}>
          <div className="flex">
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
         </div>
         <div className="flex">
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
         </div>
         <div className="flex">
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
         </div>
         <div className="flex">
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
         </div>
         <div className="flex">
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
         </div>
         <div className="flex">
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
      <Compontent />
         </div>
      </SelectableGroup>
    </div>
  );
}

export default App;

import { createSelectable } from 'react-selectable-fast'


const Compontent= ({selectableRef, isSelected, isSelecting})=> {
    let className =`w-20 h-20 mx-2 my-2 border-2 border-black ${isSelecting ? 'bg-green-200' : isSelected ? 'bg-red-400' : ''} `
    return (<div className={className} ref={selectableRef}>
    </div>)
}

export default createSelectable(Compontent)